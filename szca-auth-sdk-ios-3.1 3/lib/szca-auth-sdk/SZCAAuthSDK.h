//
//  SZCAAuthSDK.h
//  FBSnapshotTestCase
//
//  Created by wizard_yl on 30/3/18.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SZCAAuthInfo.h"
@class SZCAReqApplyModel;

typedef enum
{
    SZCA_AUTH_INIT_SUCCES_HAS_SIGN_KEY,
    SZCA_AUTH_INIT_SUCCES_NO_SIGN_KEY,
    SZCA_AUTH_INIT_FAILURE_PIN_ERROR,
    SZCA_AUTH_INIT_FAILURE_ERROR,
}SZCA_AUTH_INIT_ENUM;

@interface SZCAAuthSDK : NSObject


+(instancetype)SZCAAuthShared;

/**
 验证SDKLicense
 
 @param license license签名
 @return 返回1，验证成功，其他，失败
 */
- (int)verifySDKLicense:(NSString*)license;

/**
 *  初始化
 *
 *  @param account  账号
 *  @param keytype  密钥类型   RSA/SM2
 *  @param pwd      密码
 *
 *  @return 返回 SZCA_AUTH_INIT_FAILURE_ERROR，初始化失败
 *          返回 SZCA_AUTH_INIT_FAILURE_PIN_ERROR，pin码错误，初始化失败
 *          返回 SZCA_AUTH_INIT_SUCCES_NO_SIGN_KEY，初始化成功，本地没有签名密钥
 *          返回 SZCA_AUTH_INIT_SUCCES_HAS_SIGN_KEY，初始化成功，本地存在签名密钥
 *
 *
 */
-(SZCA_AUTH_INIT_ENUM)initialize:(NSString*)account keyTpye:(NSString*)keytype pwd:(NSString*)pwd;

/**
 *  清除内存数据，使用本方法后需重新初始化
 */
-(void)finalize;

/**
 *设置请求的baseURL
 *
 *@param baseURL baseURL
 *
 */
- (void)setBaseURL:(NSString*)baseURL;

/**
 *检测SDK是否可用
 *
 *@param error error
 *@return YES:SDK状态可用，NO:SDK未验证license或者未初始化
 */
-(BOOL)isSDKValid:(NSError**)error;

/**
 *身份认证接口，认证成功后申请证书
 *
 *@param viewController 当前的视图控制器
 *@param completion 完成的回调block
 */
-(void)authIdentityWithController:(UIViewController*)viewController
                       completion:(void(^)(SZCAAuthInfo*  authInfo,NSError *error))completion;

/**
 证书遗失补办

 @param viewController 当前的视图控制器
 @param completion 完成的回调block
 */
-(void)reissueCertWithController:(UIViewController*)viewController
                      completion:(void(^)(SZCAAuthInfo*  authInfo,NSError *error))completion;

/**
 重置Pin码

 @param viewController 当前的视图控制器
 @param account 账户名
 @param completion 完成的回调block
 */
-(void)resetPinWithController:(UIViewController*)viewController
                      account:(NSString*)account
                   completion:(void(^)(NSError *error))completion;
/**
 *签署文件的接口
 *
 *@param fileId 待签署文件的id
 *@param hash 待签名文件数据的hash，base64编码
 *@param completion 请求成功的回调block
 *@param failure 请求失败的回调block
 *
 */
- (void)signFileWithFileId:(NSString *)fileId
                      hash:(NSString*)hash
                    hasTsa:(NSString*)hasTsa
                completion:(void(^)(NSData*  signedFile))completion
                   failure:(void(^)(NSError *error))failure;
/**
 *  获取证书序列号
 *
 *
 *  @return 证书序列号
 */
-(NSString*)getCertSn;
/**
 *  获取证书主题
 *
 *
 *  @return 证书主题
 */
-(NSString*)getCertDn;
/**
 *  存储证书序列号
 *
 *  @param serialNum 证书序列号
 *
 *  @return 0:成功，其他:失败
 */
- (int)setCertSN:(NSString *)serialNum;
/**
 *  存储证书主题
 *
 *  @param certDn 证书主题
 *
 *  @return 0:成功，其他:失败
 */
- (int)setCertDN:(NSString *)certDn;
/**
 生成p10
 
 @param suject 证书主题
 @return p10
 */
- (NSString*)createP10WithSuject:(NSString*)suject;


/**
 证书申请或遗失补办请求

 @param model 请求的model ,model.token可以传空
 @param completionBlock 请求成功的回调block
 @param failureBlock 请求失败的回调block
 */
-(void)requestCert:(SZCAReqApplyModel*)model
        completion:(void(^)(NSString *certDN,NSString *certSN))completionBlock
           failure:(void(^)(NSError* error))failureBlock;
@end
