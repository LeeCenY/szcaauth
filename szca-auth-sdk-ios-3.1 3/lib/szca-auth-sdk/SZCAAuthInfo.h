//
//  SZCAAuthInfo.h
//  Pods
//
//  Created by wizard_yl on 17/4/18.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class SZCAIDCardInfo;
@interface SZCAAuthInfo : NSObject
//银行卡号
@property(nonatomic,strong)NSString *bankCard;
//银行卡预留电话号码
@property(nonatomic,strong)NSString *phoneNumber;
//手写签名图片
@property(nonatomic,strong)UIImage *handWrittenImg;
//身份证信息
@property(nonatomic,strong)SZCAIDCardInfo *idCardInfo;
//签署token
@property(nonatomic,strong)NSString *token;
@property(nonatomic,strong)NSString *certDn;
@property(nonatomic,strong)NSString *certSn;
@end
