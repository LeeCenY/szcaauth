//
//  SZCAReqApplyModel.h
//  Pods
//
//  Created by wizard_yl on 8/8/18.
//

#import <Foundation/Foundation.h>

@interface SZCAReqApplyModel : NSObject

//姓名
@property(nonatomic,strong)NSString *userName;
//身份证号码
@property(nonatomic,strong)NSString *idNo;
//操作令牌,可选参数
@property(nonatomic,strong)NSString *token;
//性别
@property(nonatomic,strong)NSString *sex;
//手机号
@property(nonatomic,strong)NSString *mobileNo;
//身份证正面, base64编码
@property(nonatomic,strong)NSString *identityImgOne;
//身份证反面, base64编码
@property(nonatomic,strong)NSString *identityImgTwo;
//签名图片, base64编码
@property(nonatomic,strong)NSString *signImg;
//活体检测图片, base64编码
@property(nonatomic,strong)NSString *humanBodyImg;
//身份证上省份
@property(nonatomic,strong)NSString *province;
//身份证上城市
@property(nonatomic,strong)NSString *city;
//身份证上联系地址
@property(nonatomic,strong)NSString *contactAddr;
//身份证签发机关
@property(nonatomic,strong)NSString *cardedPlace;
//身份证有效期
@property(nonatomic,strong)NSString *cardedExpiryDate;
//银行卡号，可选参数,appType为loseCert的时候可以为空
@property(nonatomic,strong)NSString *card;
//证书载体，固定值@"0"
@property(nonatomic,strong)NSString *carrier;
//申请类型，值为"apply"代表新申请， 值为"loseCert"代表遗失补办
@property(nonatomic,strong)NSString *appType;

@end
