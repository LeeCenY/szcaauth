//
//  SZCAIDCardInfo.h
//  Pods
//
//  Created by wizard_yl on 8/4/18.
//

#import <Foundation/Foundation.h>

@interface SZCAIDCardInfo : NSObject

//身份证姓名
@property(nonatomic,copy)NSString *name;
//身份证号码
@property(nonatomic,copy)NSString *idNo;
//性别
@property(nonatomic,copy)NSString *sex;
//民族
@property(nonatomic,copy)NSString *race;
//出生日期
@property(nonatomic,copy)NSString *birthDay;
//身份证上地址
@property(nonatomic,copy)NSString *address;
//身份证正面照片，base64编码
@property(nonatomic,strong)NSString *imgCardFront;
//身份证反面照片，base64编码
@property(nonatomic,strong)NSString *imgCardBack;
//身份证颁发机构
@property(nonatomic,copy)NSString *issue;
//身份证有效期
@property(nonatomic,copy)NSString *period;

@end
