//
//  ViewController.m
//  szca-auth-demo
//
//  Created by wizard_yl on 11/4/18.
//  Copyright © 2018年 wizard_yl. All rights reserved.
//

#import "ViewController.h"
#import "SZCAAuthSDK.h"


//#define BASE_URL @"http://218.17.161.11:18008/"
#define BASE_URL @"http://218.17.161.11:9085/"
#define LICENSE_KEY @"aeff6722b3a28980861b32760adfced62a76858de71fc7a35a78022ccea37ea68c1ba864ea1b67e3899cd2e1a20d11b4a9b1ccde62e82164aee1fe8654892b9b35ef47d3ca3c9a377051502bb2c9e37e7754ff06149c7447fdb5dfd3c8961c7c31f5c15f76f091d8e2a785327bc28346c0bc151df9afa66054df4391d12807b8492659d0600ed419629bf2e0ba15dad35d23b5598a0e1f4a58ecca6fc1d01c87e2e233ea0a8517e01dd8f304d4e9911f6071a3af09a1c5f8eb63a278d0e9fdc285923d3529e382a22782fa2ffa3105e533aad86fa66dc3deaf681fba7ab6aa6802d639d813895ef9b48461ce2f78f9c657d2d89c04a3aaba1fc8938867484bb4d7b08fe744d176eb5e232c1637c7c32706440a00dfa751bb195e43b15f94a0a6ff016248d4f010fbcdaaf4e9008e7478b1a00bb89f69344e18d7987cc6abc97ac6869e06f44b11a7295a70ed3ed7d86cd59ce5801e9f4bf932fdc0be8808da74f0917014c4406bf258edcfa0d250c7895dafabb3b8b8eff1d5b46d3d1b545827b1ce5dde1304ec411fe318fd9804365f15a21513405d688051dece5fdeedfdc19a75ccb3e890605a9a304212276faf8bea278760253f544523118842d309b320c579a2572c16d8d9728713eb2562be0d169e72ca61cf043d6d7006af3bcde14a1ea6067eab36c353bf22deff1ee1a6fb9732a5ccdab1a6c2709f825f3b7bffde"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    SZCAAuthSDK *sdk = [SZCAAuthSDK SZCAAuthShared];
    int ret = [sdk verifySDKLicense:LICENSE_KEY];
    if (ret != 1) {
        [self showAlerWithText:@"license验证失败"];
    }else{
        [sdk setBaseURL:BASE_URL];
        
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)initSDK:(id)sender{
    SZCAAuthSDK *sdk = [SZCAAuthSDK SZCAAuthShared];
    
    SZCA_AUTH_INIT_ENUM initCode= [sdk initialize:@"421083198501225619" keyTpye:@"RSA" pwd:@"123456"];
    if (SZCA_AUTH_INIT_SUCCES_NO_SIGN_KEY == initCode) {
        [self showAlerWithText:@"初始化成功，本地没有签名密钥"];
    }else if (SZCA_AUTH_INIT_SUCCES_HAS_SIGN_KEY == initCode){
        [self showAlerWithText:@"初始化成功，本地有签名密钥"];
    }else{
        [self showAlerWithText:@"初始化失败"];
    }
}

- (IBAction)authAction:(id)sender{
    SZCAAuthSDK *authSDK = [SZCAAuthSDK SZCAAuthShared];
    [authSDK authIdentityWithController:self completion:^(SZCAAuthInfo*  authInfo,NSError *error) {
        if (authInfo) {
            [self showAlerWithText:[NSString stringWithFormat:@"certSn:%@,certDn = %@",authInfo.certSn,authInfo.certDn]];
        }
    }];
}

- (IBAction)signFileAction:(id)sender{
    SZCAAuthSDK *sdk = [SZCAAuthSDK SZCAAuthShared];
    NSString *fileId = @"201808211521372137678125";
    NSString *hash = @"DoI+tbTaMwIJNFknVZc1VwNuPH4=";
    NSString *hasTsa = @"0";
    [sdk signFileWithFileId:fileId hash:hash hasTsa:hasTsa completion:^(NSData *signedFile) {
        if (signedFile) {
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,  NSUserDomainMask, YES);
            NSString *docDir = [paths objectAtIndex:0];
            NSData *data = [[NSData alloc] initWithBase64EncodedData:signedFile options:0];
            NSString *filepath = [NSString stringWithFormat:@"%@/signed01.pdf",docDir];
            [data writeToFile:filepath atomically:NO];
            NSLog(@"signedFile:%@",signedFile);
        }
    } failure:^(NSError *error) {
        if (error) {
            NSLog(@"error:%@",error.description);
        }
    }];
}

- (IBAction)resetPinAction:(id)sender{
    SZCAAuthSDK *authSDK = [SZCAAuthSDK SZCAAuthShared];
    [authSDK resetPinWithController:self account:@"421083198501225619" completion:^(NSError *error) {
        [self showAlerWithText:@"重置PIN成功"];
    }];
}
- (IBAction)reissueAction:(id)sender{
    SZCAAuthSDK *authSDK = [SZCAAuthSDK SZCAAuthShared];
    [authSDK reissueCertWithController:self completion:^(SZCAAuthInfo *authInfo, NSError *error) {
        if (error) {
            [self showAlerWithText:error.domain];
        }
        if (authInfo) {
            [self showAlerWithText:[NSString stringWithFormat:@"certSn:%@,certDn = %@",authInfo.certSn,authInfo.certDn]];
        }
    }];
}

-(void)showAlerWithText:(NSString*)msg {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:okAction];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertController animated:YES completion:nil];
    });
}
@end
