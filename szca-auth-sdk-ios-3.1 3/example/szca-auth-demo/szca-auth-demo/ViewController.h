//
//  ViewController.h
//  szca-auth-demo
//
//  Created by wizard_yl on 11/4/18.
//  Copyright © 2018年 wizard_yl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

- (IBAction)initSDK:(id)sender;
- (IBAction)authAction:(id)sender;
- (IBAction)signFileAction:(id)sender;
- (IBAction)resetPinAction:(id)sender;
- (IBAction)reissueAction:(id)sender;
@end

