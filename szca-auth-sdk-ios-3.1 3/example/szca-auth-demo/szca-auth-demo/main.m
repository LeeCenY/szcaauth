//
//  main.m
//  szca-auth-demo
//
//  Created by wizard_yl on 11/4/18.
//  Copyright © 2018年 wizard_yl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
