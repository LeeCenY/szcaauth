//
//  AppDelegate.h
//  szca-auth-demo
//
//  Created by wizard_yl on 11/4/18.
//  Copyright © 2018年 wizard_yl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

